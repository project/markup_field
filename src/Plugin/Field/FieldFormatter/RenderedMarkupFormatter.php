<?php

namespace Drupal\markup_field\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Render\RendererInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin for Ingredient formatter.
 *
 * @FieldFormatter(
 *   id = "markup_field_rendered_markup",
 *   label = @Translation("Rendered markup"),
 *   field_types = {
 *     "markup_field"
 *   }
 * )
 */
class RenderedMarkupFormatter extends FormatterBase implements ContainerFactoryPluginInterface {

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * {@inheritdoc}
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, RendererInterface $renderer) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('renderer')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    /** @var \Drupal\markup_field\Plugin\Field\FieldType\MarkupFieldItem[] $items */
    foreach ($items as $delta => $item) {
      // Add rendered markup.
      if (!empty($item->markup)) {
        $elements[$delta]['markup'] = [
          '#type' => 'inline_template',
          '#template' => $item->markup,
        ];
      }
      // Add related css/js assets.
      if (!empty($item->assets)) {
        $assets = $item->assets;
        $rendered_assets = $this->renderer->renderInIsolation($assets);
        $elements[$delta]['assets'] = [
          '#type' => 'inline_template',
          '#template' => (string) $rendered_assets,
        ];
      }
    }
    return $elements;
  }

}
